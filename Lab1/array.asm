.data 

array: 		.word 1,2,3,4,5
array_size:	.word 5
		#i,j,n
		#tablero[i][j] = tablero[i*n + j]
tablero:	.word 3, 0, 1,
		      2, 6, -2,
		      4, 7, 3
n:	.word 3 #nxn cells

#macros

.macro EXIT_PROGRAM
	addi $v0, $zero, 10
	syscall                     # llamada a sistema para terminar ejecucion del programa syscall 10
.end_macro


###################  
# GET SET ARREGLO #
###################
# retorno en registro $a0
# ingreso el index 0 a (n-1) del arreglo
.macro GET_TABLERO(%X)
    la $t3, tablero        
    move $t2, %X           
    add $t2, $t2, $t2    
    add $t2, $t2, $t2    
    add $t1, $t2, $t3    
    lw $a0, 0($t1)       # get
.end_macro
# i = $a0
# %X = value for set    
.macro SET_TABLERO(%X)
    la $t3, tablero
    move $t2, $a0   
    add $t2, $t2, $t2    
    add $t2, $t2, $t2    
    add $t1, $t2, $t3  
    move $a0, %X  
    sw $a0, 0($t1)       # set
.end_macro
#guarda en $s7 el contenido de la celda tablero[i][j]
# i = $a0
# j = $a1
# tablero[i][j] = $a0
.macro TABLERO_i_j
    la $t3, tablero         	# cargar tablero en $t3
    lw $t6, n			# matriz n x n
    move $t4, $a0           	# i
    move $t2, $a1		# j
    add $t4, $t4, $t4		# i = i*2
    add $t4, $t4, $t4		# i = i*2 => (i*4)
    add $t2, $t2, $t2		# j = j*2
    add $t2, $t2, $t2		# j = j*2 => (j*4)
    mul $t4, $t4, $t6		# i = i*n
    add $t4, $t4, $t2		# i = i*n + j
    add $t1, $t4, $t3    	# 
    lw $a0, 0($t1)       	# 
.end_macro    
    
.macro PRINT_int_a0(%X)
	move $a0,%X
 	li $v0, 1
	syscall
.end_macro 

.macro PRINT_string_a0(%X)
	la $a0, %X
 	li $v0, 4
	syscall
.end_macro 

endl:	.asciiz	"\n"
tab:	.asciiz	"\t"

.text

main:	
	li $a0,1
	li $a1,2
	TABLERO_i_j
	move $s0,$a0
	PRINT_int_a0($s0)
	PRINT_string_a0(endl)
	addi $s0,$s0,1000
		
	PRINT_int_a0($s0)
	PRINT_string_a0(endl)
	li $t0,0 
	GET_TABLERO($t0)
	move $t1,$a0
	li $a0,0
	li $t6, 11
	SET_TABLERO($t6)
	li $a0,0
	li $a1,0
	TABLERO_i_j
	move $s0,$a0
	PRINT_int_a0($s0)
	
	
	
EXIT_PROGRAM

