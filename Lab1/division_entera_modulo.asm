.data
endl:	.asciiz	"\n"
tab:	.asciiz	"\t"

###################  
#  PRINT_STANDART #
###################
.macro PRINT_int_register(%X)
	move $a0,%X
 	li $v0, 1
	syscall
.end_macro

.macro PRINT_string_asciiz(%X)
	la $a0, %X
 	li $v0, 4
	syscall
.end_macro 

###################  
#  Div & modulo   #
###################
.macro division_reg1_reg2(%reg1, %reg2)
	div $a0, %reg1, %reg2
.end_macro

.macro modulo_reg1_reg2(%reg1, %reg2)
	div $t0, %reg1, %reg2
	mul $t0, $t0, -1	# - parte entera
	mul $t0, $t0, %reg2
	add $a0, %reg1, $t0	# numero-parte entera = resto
.end_macro

.text



###################  
#      MAIN 	  #
###################
main:
	li $a1, 121
	li $a2, 3
	division_reg1_reg2($a1,$a2)
	PRINT_int_register($a0)
	PRINT_string_asciiz(endl)
	
	modulo_reg1_reg2($a1,$a2)
	PRINT_int_register($a0)
	PRINT_string_asciiz(endl)
