##################################################################  
# ISRAEL MARTINEZ # ORGANIZACIÓN DE COMPUTADORES 1 SEMESTRE 2015 #
##################################################################

###################  
# 	DATA 	  #
###################

.data

display:	.word 0:131072
pared:		.word -1
comida:		.word -2
digito:		.word -3
fondo_digito:	.word -4
max_pared:	.word 10
seed:		.word 10
modo_juego:	.word 1
n:		.word 32 #largo 32 tablero de juego
# 32x32 = 1024 + 224 = 1248 (224 = for digit display)
maxPixel:	.word 1248

cabeza_i:		.word 0
cabeza_j:		.word 0

start:			.word 0

score:			.word 0

endl:	.asciiz	"\n"
tab:	.asciiz	"\t"
ingrese_seed:		.asciiz "Para generación aleatoria de obstáculos: \n Ingrese un número entero por favor \n"
ingrese_max_pared:	.asciiz "Seleccione maxima cantidad de obstáculos (MAX 50)\n"
ingrese_modo_juego:	.asciiz "Seleccione modo de juego presione \n 1: Normal \n 2: Crazy \n"
otra_partida:		.asciiz "¿Desea comenzar otra partida? presione \n 1: reiniciar partida \n 2: Nueva partida \n Otro número: salir\n"
GAME_OVER:		.asciiz "GAME_OVER\n"		      


###################  
#     TABLERO 	  #
###################
tablero:	.word 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0

-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4
-4,-3,-3,-3,-4,-3,-3,-3,-4,-3,-3,-3,-4,-3,-3,-3,-4,-4,-4,-4,-3,-3,-3,-4,-3,-3,-3,-4,-3,-3,-3,-4
-4,-4,-3,-4,-4,-3,-4,-4,-4,-3,-4,-3,-4,-3,-4,-3,-4,-4,-4,-4,-3,-4,-3,-4,-3,-4,-3,-4,-3,-4,-3,-4
-4,-4,-3,-4,-4,-3,-3,-3,-4,-3,-3,-3,-4,-3,-3,-3,-4,-4,-4,-4,-3,-4,-3,-4,-3,-4,-3,-4,-3,-4,-3,-4
-4,-4,-3,-4,-4,-4,-4,-3,-4,-3,-3,-4,-4,-3,-4,-3,-4,-4,-4,-4,-3,-4,-3,-4,-3,-4,-3,-4,-3,-4,-3,-4
-4,-3,-3,-3,-4,-3,-3,-3,-4,-3,-4,-3,-4,-3,-4,-3,-4,-4,-4,-4,-3,-3,-3,-4,-3,-3,-3,-4,-3,-3,-3,-4
-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4

###################  
#  PRINT_STANDART #
###################

.macro READ_int
	li $v0, 5	# read int
	syscall
.end_macro
.macro PRINT_int_register(%X)
	move $a0,%X
 	li $v0, 1
	syscall
.end_macro

.macro PRINT_string_asciiz(%X)
	la $a0, %X
 	li $v0, 4
	syscall
.end_macro 

###################  
#  Div & modulo   #
###################
.macro division_reg1_reg2(%reg1, %reg2)
	div $a0, %reg1, %reg2
.end_macro

.macro modulo_reg1_reg2(%reg1, %reg2)
	div $t0, %reg1, %reg2
	mul $t0, $t0, -1	# - parte entera
	mul $t0, $t0, %reg2
	add $a0, %reg1, $t0	# reg1 - parte_entera*reg2 = resto
.end_macro

###################  
# score_plus_plus #
###################
.macro score_plus_plus
	lw $t0, score
	addi $t0, $t0, 1
	sw $t0, score
.end_macro

###################  
# reg_plus_plus   #
###################
.macro a0_plus_plus
	addi $a0, $a0, 4      
.end_macro

.macro s0_plus_plus
	addi $s0, $s0, 4
.end_macro

###################  
# paint_a3_COLORS #
###################
.macro paint_a3_RED
	addi $a3, $zero, 0x00ff0000 
.end_macro

.macro paint_a3_GREEN
	addi $a3, $zero, 0x0000ff00 
.end_macro

.macro paint_a3_DARK_GREEN
	addi $a3, $zero, 0x00006400 
.end_macro
.macro paint_a3_BLUE
	addi $a3, $zero, 0x000000ff
.end_macro

.macro paint_a3_ORANGE
	addi $a3, $zero, 0x00ff8c00
.end_macro

.macro paint_a3_BLACK
	addi $a3, $zero, 0x00000000
.end_macro

.macro paint_a3_WHITE
	addi $a3, $zero, 0x00ffffff
.end_macro

.macro drawPixel_a3_to_a0
	sw $a3, 0($a0)                      # copiamos el color en $a3 a la direccion de memoria del display
.end_macro


###################  
#    SET values   #
###################
.macro SET_max_pared(%max)
	la $t0, max_pared
	sw %max, 0($t0)
.end_macro
.macro SET_seed(%seed)
	la $t0, seed
	sw %seed, 0($t0)
.end_macro

.macro SET_modo_juego(%modo)
	la $t0, modo_juego
	sw %modo, 0($t0)
.end_macro

.macro SET_score(%score)
	la $t0, score
	sw %score, 0($t0)
.end_macro


###################  
# SET GET CABEZA  #
###################

.macro GET_CABEZA_i
	la $t0, cabeza_i
	lw $a0, 0($t0)
.end_macro

.macro SET_CABEZA_i(%X)
	la $t0, cabeza_i
	sw %X, 0($t0)
.end_macro
.macro GET_CABEZA_j
	la $t0, cabeza_j
	lw $a0, 0($t0)
.end_macro

.macro SET_CABEZA_j(%X)
	la $t0, cabeza_j
	sw %X, 0($t0)
.end_macro

.macro GET_CABEZA_i_j
	GET_CABEZA_i
	move $t1, $a0
	GET_CABEZA_j
	move $a1, $a0
	move $a0, $t1
.end_macro
###################  
# GET SET ARREGLO #
###################
# retorno en registro $a0
# ingreso el index 0 a (n-1) del arreglo
.macro GET_TABLERO(%X)
    la $t3, tablero        
    move $t2, %X           
    add $t2, $t2, $t2    
    add $t2, $t2, $t2    
    add $t1, $t2, $t3    
    lw $a0, 0($t1)       # get
.end_macro
# i = $a0
# %X = value for set    
.macro SET_TABLERO(%X)
    la $t3, tablero
    move $t2, $a0   
    add $t2, $t2, $t2    
    add $t2, $t2, $t2    
    add $t1, $t2, $t3  
    move $a0, %X  
    sw $a0, 0($t1)       # set
.end_macro

###################  
#   GET_TABLERO_i_j   #
###################
#guarda en $a0 el contenido de la celda tablero[i][j]
# i = $a0
# j = $a1
# $a0 = tablero[i][j]
.macro GET_TABLERO_i_j
    la $t3, tablero         	# cargar tablero en $t3
    lw $t6, n			# matriz n x n
    move $t4, $a0           	# i
    move $t2, $a1		# j
    add $t4, $t4, $t4		# i = i*2
    add $t4, $t4, $t4		# i = i*2 => (i*4)
    add $t2, $t2, $t2		# j = j*2
    add $t2, $t2, $t2		# j = j*2 => (j*4)
    mul $t4, $t4, $t6		# i = i*n
    add $t4, $t4, $t2		# i = i*n + j
    add $t1, $t4, $t3    	# 
    lw $a0, 0($t1)       	# get
.end_macro

###################  
# SET_TABLERO_i_j #
###################
#Seteamos la posicion tablero[i][j]
# i = $a0
# j = $a1
# tablero[i][j] = %X   %X = registro que tiene el entero a setear en la matriz
.macro SET_TABLERO_i_j(%X)
    la $t3, tablero         	# cargar tablero en $t3
    lw $t6, n			# matriz n x n
    move $t4, $a0           	# i
    move $t2, $a1		# j
    add $t4, $t4, $t4		# i = i*2
    add $t4, $t4, $t4		# i = i*2 => (i*4)
    add $t2, $t2, $t2		# j = j*2
    add $t2, $t2, $t2		# j = j*2 => (j*4)
    mul $t4, $t4, $t6		# i = i*n
    add $t4, $t4, $t2		# i = i*n + j
    add $t1, $t4, $t3    	# 
    move $t0, %X			# $a0 = %X
    sw $t0, 0($t1)       	# tablero[i][j] = $a0
.end_macro   

###################  
#  GENERATE DIGIT #
###################
#	lw $s0, digito 
#	lw $s1, fondo_digito
#	li $s3, 0
#	move $a0, %i
#	move $a1, %j
#	set_3pixel($s0, $s0, $s0)
#	set_3pixel($s0, $s0, $s0)
#	set_3pixel($s0, $s0, $s0)
#	set_3pixel($s0, $s0, $s0)
#	set_3pixel($s0, $s0, $s0)
.macro set_3pixel(%set1, %set2, %set3)
	move $t7, %set1
	move $t8, %set2
	move $t9, %set3
	SET_TABLERO_i_j($t7)	
	addi $a1, $a1, 1	# j+1
	SET_TABLERO_i_j($t8)
	addi $a1, $a1, 1	# j+1
	SET_TABLERO_i_j($t9)
	addi $a0, $a0, 1	# i+1
	addi $a1, $a1, -2	# reset j
.end_macro

.macro digit_0(%i, %j)
	lw $s0, digito 
	lw $s1, fondo_digito
	li $s3, 0
	move $a0, %i
	move $a1, %j
	set_3pixel($s0, $s0, $s0)
	set_3pixel($s0, $s1, $s0)
	set_3pixel($s0, $s1, $s0)
	set_3pixel($s0, $s1, $s0)
	set_3pixel($s0, $s0, $s0)
.end_macro

.macro digit_1(%i, %j)
	lw $s0, digito 
	lw $s1, fondo_digito
	li $s3, 0
	move $a0, %i
	move $a1, %j
	set_3pixel($s1, $s0, $s1)
	set_3pixel($s0, $s0, $s1)
	set_3pixel($s1, $s0, $s1)
	set_3pixel($s1, $s0, $s1)
	set_3pixel($s0, $s0, $s0)
.end_macro
.macro digit_2(%i, %j)
	lw $s0, digito 
	lw $s1, fondo_digito
	li $s3, 0
	move $a0, %i
	move $a1, %j
	set_3pixel($s0, $s0, $s0)
	set_3pixel($s1, $s1, $s0)
	set_3pixel($s0, $s0, $s0)
	set_3pixel($s0, $s1, $s1)
	set_3pixel($s0, $s0, $s0)
.end_macro
.macro digit_3(%i, %j)
	lw $s0, digito 
	lw $s1, fondo_digito
	li $s3, 0
	move $a0, %i
	move $a1, %j
	set_3pixel($s0, $s0, $s0)
	set_3pixel($s1, $s1, $s0)
	set_3pixel($s0, $s0, $s0)
	set_3pixel($s1, $s1, $s0)
	set_3pixel($s0, $s0, $s0)
.end_macro
.macro digit_4(%i, %j)
	lw $s0, digito 
	lw $s1, fondo_digito
	li $s3, 0
	move $a0, %i
	move $a1, %j
	set_3pixel($s0, $s1, $s0)
	set_3pixel($s0, $s1, $s0)
	set_3pixel($s0, $s0, $s0)
	set_3pixel($s1, $s1, $s0)
	set_3pixel($s1, $s1, $s0)
.end_macro
.macro digit_5(%i, %j)
	lw $s0, digito 
	lw $s1, fondo_digito
	li $s3, 0
	move $a0, %i
	move $a1, %j
	set_3pixel($s0, $s0, $s0)
	set_3pixel($s0, $s1, $s1)
	set_3pixel($s0, $s0, $s0)
	set_3pixel($s1, $s1, $s0)
	set_3pixel($s0, $s0, $s0)
.end_macro
.macro digit_6(%i, %j)
	lw $s0, digito 
	lw $s1, fondo_digito
	li $s3, 0
	move $a0, %i
	move $a1, %j
	set_3pixel($s0, $s0, $s0)
	set_3pixel($s0, $s1, $s1)
	set_3pixel($s0, $s0, $s0)
	set_3pixel($s0, $s1, $s0)
	set_3pixel($s0, $s0, $s0)
.end_macro
.macro digit_7(%i, %j)
	lw $s0, digito 
	lw $s1, fondo_digito
	li $s3, 0
	move $a0, %i
	move $a1, %j
	set_3pixel($s0, $s0, $s0)
	set_3pixel($s1, $s1, $s0)
	set_3pixel($s1, $s0, $s0)
	set_3pixel($s1, $s1, $s0)
	set_3pixel($s1, $s1, $s0)
.end_macro
.macro digit_8(%i, %j)
	lw $s0, digito 
	lw $s1, fondo_digito
	li $s3, 0
	move $a0, %i
	move $a1, %j
	set_3pixel($s0, $s0, $s0)
	set_3pixel($s0, $s1, $s0)
	set_3pixel($s0, $s0, $s0)
	set_3pixel($s0, $s1, $s0)
	set_3pixel($s0, $s0, $s0)
.end_macro
.macro digit_9(%i, %j)
	lw $s0, digito 
	lw $s1, fondo_digito
	li $s3, 0
	move $a0, %i
	move $a1, %j
	set_3pixel($s0, $s0, $s0)
	set_3pixel($s0, $s1, $s0)
	set_3pixel($s0, $s0, $s0)
	set_3pixel($s1, $s1, $s0)
	set_3pixel($s0, $s0, $s0)
.end_macro


###################  
#     RANDOM      #
###################

.macro GET_TIME_SYSTEM
	addi $v0, $zero, 30
	syscall
.end_macro

.macro SET_SEED(%X)
	add $a0, $a0, %X
	addi $v0, $zero, 40
	syscall
.end_macro

.macro RANDOM_SEED_1_to_30(%reg)  
	move $a0, %reg
	li $a1, 30	# max =  value - 1  (-1 por emzar de 1)
	li $v0, 42   	#random
	syscall
	addi $a0, $a0, 1 #para empezar de 1 a 30
.end_macro

.macro RANDOM_SEED_0_to_31(%reg)  
	move $a0, %reg
	li $a1, 32	# max =  value - 1 
	li $v0, 42   	#random
	syscall
.end_macro

.macro	RANDOM_i_j_a0_a1(%seed)
	move $a3, %seed
	RANDOM_SEED_0_to_31($a3)
	move $s0, $a0 
	add $a3, $a3, $s0
	RANDOM_SEED_0_to_31($a3)
	move $a1, $a0		# retornar j
	move $a0, $s0		# retornar i
.end_macro

###################  
#   RANDOM MAP    #
###################

.macro generate_random_snake(%seed)
	# generar cabeza del snake de manera aleatoria y completar hacia la izq setPixel
	move $a0, %seed
	RANDOM_i_j_a0_a1($a0)
	SET_CABEZA_i($a0)
	SET_CABEZA_j($a1)
	li $t0, 3
	SET_TABLERO_i_j($t0)	# cabeza del snake
	addi $a1, $a1, -1	# dibujar cuerpo snake
	li $t0, 3
	SET_TABLERO_i_j($t0)
	addi $a1, $a1, -1	# dibujar cuerpo snake
	li $t0, 3
	SET_TABLERO_i_j($t0)
.end_macro

#.macro generate_random_comida(%seed)

#.end_macro

.macro generate_random_pared(%seed)
	# generar obstaculos
	move $a0, %seed
	RANDOM_i_j_a0_a1($a0)
.end_macro

###################  
#  EXIT_PROGRAM	  #
###################
.macro EXIT_PROGRAM
	addi $v0, $zero, 10
	syscall                    
.end_macro

###################  
# 	TEXT 	  #
###################
.text

###################  
# 	MAIN 	  #
###################
#Unit Width in Pixel 16
#Unit Height in Pixel 16
#Display Width in Pixel 512
#Display Height in Pixel 512 /1024 for score
# code ascii keyboard
# a = 97 
# s = 115
# d = 100
# w = 119
# q = 113
main:	
	#semilla entera por consola
	PRINT_string_asciiz(ingrese_seed)
	READ_int	# return v0
	move $a0, $v0
	SET_seed($a0)
	PRINT_string_asciiz(ingrese_modo_juego)
	READ_int	# return v0
	move $a1, $v0
	SET_modo_juego($a1)
	PRINT_string_asciiz(ingrese_max_pared)
	READ_int	# return v0
	move $a2, $v0
	bge $a2, 51, set50	# max 50
	j end_set50
	set50:
		li $a2, 50
		SET_max_pared($a2)
	end_set50:
	SET_max_pared($a2)
	
	#re_init:
	#lw $a0, seed
	#lw $a1, modo_juego
	#lw $a2, max_pared
	#li $t1, 0
	#SET_score($t1)
	beq $a1, 1, modo_normal
	j mode_crazy
		end_mode_crazy:	
		
		li $s3, 2
		j generate_random_pared
		end_generate_random_pared_crazy:
	
		generate_random_snake($a0)
		
	j end_modo		
	modo_normal:
		li $s3, 1
		j generate_random_pared
		end_generate_random_pared_normal:
	
		generate_random_snake($a0)
		# generar comida aleatoria
		RANDOM_i_j_a0_a1($a0)		
		li $s3, 2
		j new_random_food
		end_generate_random_comida:

	end_modo:
		j pre_dibujar
		end_pre_dibujar:
###################  
#     POLLING     #
###################
	polling:
		addi $a0, $zero, 1	# cada 5 ms buena velocidad
		addi $v0, $zero, 32     # esperamos (syscall 32 sleep)
		syscall			        # esperamos 100ms para no utilizar todo el procesador

		addi $t0, $zero, 0xffff0004		# los caracteres del teclado se guardan en la dir de mem 0xffff0004
		lw $t1, 0($t0)
		#sw $zero, 0($t0)		        # Seteamos a NULL (cero) el buffer para no leer datos duplicados
		beq $t1, 97, validar_mov_izq 	# a izquierda
		beq $t1, 100, validar_mov_der 	# d derecha
		beq $t1, 119, validar_mov_arr 	# w arriba
		beq $t1, 115, validar_mov_abj 	# s abajo 	
		beq $t1, 113, exit_game 	# q salir del juego
		addi $s2, $zero, 0		#no es una tecla del juego
		
		end_validar_mov:
		beq $s2, 0, end_dibujar		#no se dibuja ni se mueve sin presionar al menos 1 vez una tecla
		
		j mov_matriz
		mov_complete:
		
		j dibujar
		end_dibujar:
		
	
	j polling
		
###################  
#    END MAIN 	  #
###################
exit_game:
	EXIT_PROGRAM


###################  
#  RANDOM PARED   #
###################
generate_random_pared:
	lw $s4, max_pared
	lw $s5, comida
	lw $s6, pared
	li $s7, 0
		
	for_random_pared:
		beq $s1, $s4, end_for_random_pared 
		move $a0, $s0
		move $a1, $s1
		RANDOM_i_j_a0_a1($s1)
		SET_TABLERO_i_j($s6)
		addi $s1, $s1, 1	# i++
	j for_random_pared
	end_for_random_pared:
beq $s3, 1, end_generate_random_pared_normal
beq $s3, 2, end_generate_random_pared_crazy

###################  
#  VALIDAR MOV    #
###################
validar_mov_izq:
	# j-1
	GET_CABEZA_i
	move $s0,$a0
	GET_CABEZA_j
	move $s1,$a0
		
	move $a0,$s0
	move $a1,$s1
	GET_TABLERO_i_j
	move $s3, $a0	 		# valor cabeza antes de mover snake
	
	beq $s1, 0, jmenor  		# si j == 32 debe aparecer al otro lado con igual i pero j = 0
	j end_jmenor			# else: no es un caso de borde, ejecucion normal
	jmenor:
		addi $s1, $zero, 32	# cabeza en el lado contrario (32 - 1 = 31 = j)
	end_jmenor:
	
	addi $s1,$s1, -1			# j = j-1
	
	move $a0, $s0
	move $a1, $s1
	GET_TABLERO_i_j			
	move $s4, $a0	 		# valor posicion siguiente
	
	addi $t0, $s3, -1 		# si sig == cabeza-1
	beq $s4, $t0, validar_mov_der	# ir a la derecha 
	
	addi $s3, $s3, 1		# mov snake +1
	SET_CABEZA_j($s1)		# nueva posicion de la cabeza
	
	move $a0, $s0
	move $a1, $s1
	SET_TABLERO_i_j($s3)		#set snake +1

	li $s2, 1			# se ha ingresado almenos una tecla de mov
	
	beq $s4, -2, comer_comida	#si era comida evitamos restar1 para aumentar tamaño
	beq $s4, -1, colision 
	bge $s4, 1, colision
j end_validar_mov 

validar_mov_der:
	#j+1
	GET_CABEZA_i
	move $s0,$a0
	GET_CABEZA_j
	move $s1,$a0
	
	move $a0,$s0
	move $a1,$s1
	GET_TABLERO_i_j
	move $s3, $a0	 		# valor cabeza antes de mover snake
	
	beq $s1, 31, jmayor  		# si j == 32 debe aparecer al otro lado con igual i pero j = 0
	j end_jmayor			# else: no es un caso de borde, ejecucion normal
	jmayor:
		addi $s1, $zero, -1	# cabeza en el lado contrario (-1 + 1 = 0 = j)
	end_jmayor:
	
	addi $s1,$s1, 1			# j = j+1
	
	move $a0, $s0
	move $a1, $s1
	GET_TABLERO_i_j
	move $s4, $a0	 		# valor posicion siguiente
	
	addi $t0, $s3, -1 		# si sig == cabeza-1
	beq $s4, $t0, validar_mov_izq	# ir a la izq
	
	addi $s3, $s3, 1		# mov snake +1
	SET_CABEZA_j($s1)		# nueva posicion de la cabeza
	
	move $a0, $s0
	move $a1, $s1
	SET_TABLERO_i_j($s3)
	li $s2, 1			# se ha ingresado almenos una tecla de mov
	
	beq $s4, -2, comer_comida	#si era comida evitamos restar1 para aumentar tamaño
	beq $s4, -1, colision
	bge $s4, 1, colision
j end_validar_mov	

validar_mov_arr:
	#i-1
	GET_CABEZA_i
	move $s0,$a0
	GET_CABEZA_j
	move $s1,$a0
	
	move $a0,$s0
	move $a1,$s1
	GET_TABLERO_i_j
	move $s3, $a0	 		# valor cabeza antes de mover snake
	
	beq $s0, 0, imenor  		# si j == 32 debe aparecer al otro lado con igual i pero j = 0
	j end_imenor			# else: no es un caso de borde, ejecucion normal
	imenor:
		addi $s0, $zero, 32	# cabeza en el lado contrario (32 - 1 = 31 = j)
	end_imenor:
	
	addi $s0,$s0, -1			# i = i-1
	
	move $a0, $s0
	move $a1, $s1
	GET_TABLERO_i_j
	move $s4, $a0	 		# valor posicion siguiente
	
	addi $t0, $s3, -1 		# si sig == cabeza-1
	beq $s4, $t0, validar_mov_abj	# ir a la izq
	
	addi $s3, $s3, 1		# mov snake +1
	SET_CABEZA_i($s0)		# nueva posicion de la cabeza
	
	move $a0, $s0
	move $a1, $s1
	SET_TABLERO_i_j($s3)
		
	li $s2, 1			# se ha ingresado almenos una tecla de mov
	#si i-1 es comida rutina alternativa
	beq $s4, -2, comer_comida	#si era comida evitamos restar1 para aumentar tamaño
	beq $s4, -1, colision
	bge $s4, 1, colision
j end_validar_mov	

validar_mov_abj:
	#i+1
	GET_CABEZA_i
	move $s0,$a0
	GET_CABEZA_j
	move $s1,$a0
	
	move $a0,$s0
	move $a1,$s1
	GET_TABLERO_i_j
	move $s3, $a0	 		# valor cabeza antes de mover snake
	
	beq $s0, 31, imayor  		# si j == 32 debe aparecer al otro lado con igual i pero j = 0
	j end_imayor		# else: no es un caso de borde, ejecucion normal
	imayor:
		addi $s0, $zero, -1	# cabeza en el lado contrario (32 - 1 = 31 = j)
	end_imayor:
	
	addi $s0,$s0, 1			# i = i+1
	
	move $a0, $s0
	move $a1, $s1
	GET_TABLERO_i_j
	move $s4, $a0	 		# valor posicion siguiente
	
	addi $t0, $s3, -1 		# si sig == cabeza-1
	beq $s4, $t0, validar_mov_arr	# ir a la izq
	
	addi $s3, $s3, 1		# mov snake +1
	SET_CABEZA_i($s0)		# nueva posicion de la cabeza
	
	move $a0, $s0
	move $a1, $s1
	SET_TABLERO_i_j($s3)
	
	li $s2, 1			# se ha ingresado almenos una tecla de mov

	beq $s4, -2, comer_comida	#si era comida evitamos restar1 para aumentar tamaño
	beq $s4, -1, colision
	bge $s4, 1, colision
j end_validar_mov
###################  
#    COLISION     #
###################
colision:
	PRINT_string_asciiz(GAME_OVER)
	j exit_game
###################  
#      COMIDA     #
###################
comer_comida:
	li $s3, 1		# for new_random_food
	j new_random_food
	end_new_random_food:
	
	score_plus_plus
	
	j generate_matrix_score
	end_generate_matrix_score:
j mov_complete

new_random_food:
	move $a0, $s0 			# i = $s0
	move $a1, $s1			# j = $s1
	GET_TABLERO_i_j   		#return a0 = tablero[i][j]
	RANDOM_SEED_0_to_31($a0)
	move $s0, $a0 
	add $a0, $a0, $s0,
	RANDOM_SEED_0_to_31($a0)
	move $s1, $a0
	
	lw $s5, comida
	lw $s6, pared
	
	for_i_validate_food: 			
		for_j_validate_food:
			beq $s1, 31, end_for_j_validate_food  
			move $a0, $s0
			move $a1, $s1
			GET_TABLERO_i_j
			beq $a0, -2, end_for_i_validate_food	# valido
			beq $a0, 0, end_for_i_validate_food	# valido
			addi $s1, $s1, 1	# j++
		j for_j_validate_food
		end_for_j_validate_food:
		
		mul $s4, $s0, $s1	
		beq $s4, 1024, init_for_validate_food
		li $s1, 0					#reset j = 0
		addi $s0, $s0, 1		# i++
	j for_i_validate_food
	

	
	init_for_validate_food:
		addi $s0, $zero, 0 	# reiniciar contador para seguir buscando
		addi $s1, $zero, 0
	j for_i_validate_food
	
	end_for_i_validate_food:
	
	move $a0, $s0
	move $a1, $s1
	SET_TABLERO_i_j($s5)  
	
beq $s3, 1, end_new_random_food 	
beq $s3, 2, end_generate_random_comida
###################  
#  MOVER MATRIZ   #
###################
# Recorre matriz y resta a los mayores a 0
# Se recorre el arreglo y se resta si corresponde
mov_matriz:
	li $s7, 0		# count = 0
	lw $s6, n  		# n dimension matriz
	mul $s6, $s6, $s6 	# nxn toda la matriz
	#addi $s6, $s6,-1 	# n = n - 1
	loop_mov_matriz:
		beq $s7, $s6, end_mov_matriz
		GET_TABLERO($s7)
		move $s5,$a0
		bge $a0, 1, restar1
		end_restar1:
		addi $s7, $s7, 1 	#count++
	j loop_mov_matriz		
end_mov_matriz:		
	j mov_complete # in main
	
restar1:
	addi $a0,$a0,-1
	move $s5, $a0	
	move $a0, $s7
	SET_TABLERO($s5) #set tablero[$a0] = $s5
j end_restar1	
##
mode_crazy:
	li $s7, 0		# count = 0
	lw $s6, n  		# n dimension matriz
	mul $s6, $s6, $s6 	# nxn toda la matriz
	lw $s5, comida
	#addi $s6, $s6,-1 	# n = n - 1
	loop_mode_crazy:
		beq $s7, $s6, end_loop_mode_crazy
		GET_TABLERO($s7)
		bge $a0, 0, full_comida
		j end_full_comida
		full_comida:
			move $a0, $s7
			SET_TABLERO($s5)
		end_full_comida:
		addi $s7, $s7, 1 	#count++
	j loop_mode_crazy
	end_loop_mode_crazy:				
j end_mode_crazy # in main	

###################  
# DIBUJAR TABLERO #
###################
dibujar:
		la $a0, display             # direccion de memoria del display		    
		addi $a1, $zero, 0          # en la direccion de memoria 0	    
		#li $a2, 1024		    # 32x32
		lw $a2, maxPixel		# 32x32
		la $s0, tablero		      # se apunta de nuevo al inicio de la matriz
		jal dibujar_tablero           # llamamos a dibujar tablero
j end_dibujar

pre_dibujar:
		la $a0, display             # direccion de memoria del display		    
		addi $a1, $zero, 0          # en la direccion de memoria 0
		#li $a2, 1024		    # 32x32
		lw $a2, maxPixel		# 32x32
		la $s0, tablero		      # se apunta de nuevo al inicio de la matriz
		jal dibujar_tablero           # llamamos a dibujar tablero
		
j end_pre_dibujar

dibujar_tablero:
# Dibuja Tablero
# $a0 direccion de memoria del display
# $a1 inicio de la linea
# $a2 fin de la linea
# $a3 color de pixel
	loop_dibujar_pixel:                 	    # mientras
		beq $a1, $a2, fin_dibujar_pixel     # no lleguemos al final ($a2), seguimos dibujando
		lw $t0, 0($s0)			    # tablero $s0
		#lw $t1, score
		#addi $t1, $t1, -2
		beq $t0, -1, drawPixel_pared		# dibujar si es pared
		#beq $t0, $t1, drawPixel_head_snake	# dibujar si es cabeza snake
		bge $t0, 1, drawPixel_snake		# dibujar si es snake (mayor o igual a 1)
		beq $t0, -2, drawPixel_comida		# dibujar comida
		beq $t0, 0, drawPixel_fondo		# dibujar fondo vacio
		beq $t0, -3, drawPixel_digito		# dibujar si es digito
		beq $t0, -4, drawPixel_fondo_digito	# dibujar si es fondo digito
		end_drawPixel:
		
		a0_plus_plus			    # avanzamos una palabra del registro base $a0 display
		s0_plus_plus		            # avanzamos una palabra del registro base $s0 tablero
		addi $a1, $a1, 1                    # aumentamos el contador
		j loop_dibujar_pixel                # volvemos al "while"

 	fin_dibujar_pixel:                  	    # terminamos la funcion
		jr $ra                              # retornamos
		
drawPixel_pared:
	paint_a3_RED
	drawPixel_a3_to_a0
	j end_drawPixel
			
drawPixel_snake:
	move $t4, $a0
	GET_TABLERO($a1)
	li $t5, 2
	modulo_reg1_reg2($a0, $t5)
	beq $a0, 1, paint_orange
	j paint_white
	paint_orange:
		move $a0, $t4
		paint_a3_ORANGE
		drawPixel_a3_to_a0
	j end_drawPixel	
	
	paint_white: 
		move $a0, $t4
		paint_a3_WHITE
		drawPixel_a3_to_a0
	j end_drawPixel
	
drawPixel_head_snake:
	paint_a3_WHITE
	drawPixel_a3_to_a0
	j end_drawPixel
	
drawPixel_comida:
	paint_a3_GREEN
	drawPixel_a3_to_a0
	j end_drawPixel
			
drawPixel_fondo:
	paint_a3_BLACK
	drawPixel_a3_to_a0
	j end_drawPixel
	
drawPixel_digito:
	paint_a3_GREEN
	drawPixel_a3_to_a0
	j end_drawPixel
	
drawPixel_fondo_digito:
	paint_a3_DARK_GREEN
	drawPixel_a3_to_a0
	j end_drawPixel		
	
###################  
#  DIBUJAR SCORE  #
###################	
generate_matrix_score:
	lw $t9, score
	li $t8, 100
	division_reg1_reg2($t9,$t8)
	move $a2, $a0			# centenas $a2	
	
	modulo_reg1_reg2($t9, $t8)
	move $t9, $a0			
	li $t8, 10
	division_reg1_reg2($t9, $t8)	
	move $a1, $a0			# decenas $a1
	modulo_reg1_reg2($t9, $t8)	# unidades $a0
	
	j generate_digits 		#generate digits in the matrix 
	end_generate_digits:
j end_generate_matrix_score			

generate_digits:
	# para unidades en $a0
	move $a3, $a1			# respaldo a1 
	li $s5, 1
	li $s6, 33
	li $s7, 28
	beq $a0, 0, generate_digit_0
	beq $a0, 1, generate_digit_1
	beq $a0, 2, generate_digit_2
	beq $a0, 3, generate_digit_3
	beq $a0, 4, generate_digit_4
	beq $a0, 5, generate_digit_5
	beq $a0, 6, generate_digit_6
	beq $a0, 7, generate_digit_7
	beq $a0, 8, generate_digit_8
	beq $a0, 9, generate_digit_9
	end_set_unit_digit:
	# para decenas en $a1
	li $s5, 2
	li $s6, 33
	li $s7, 24
	move $a1, $a3
	beq $a1, 0, generate_digit_0
	beq $a1, 1, generate_digit_1
	beq $a1, 2, generate_digit_2
	beq $a1, 3, generate_digit_3
	beq $a1, 4, generate_digit_4
	beq $a1, 5, generate_digit_5
	beq $a1, 6, generate_digit_6
	beq $a1, 7, generate_digit_7
	beq $a1, 8, generate_digit_8
	beq $a1, 9, generate_digit_9
	end_set_decimal_digit:
	# para centenas en $a2
	li $s5, 3
	li $s6, 33
	li $s7, 20
	beq $a2, 0, generate_digit_0
	beq $a2, 1, generate_digit_1
	beq $a2, 2, generate_digit_2
	beq $a2, 3, generate_digit_3
	beq $a2, 4, generate_digit_4
	beq $a2, 5, generate_digit_5
	beq $a2, 6, generate_digit_6
	beq $a2, 7, generate_digit_7
	beq $a2, 8, generate_digit_8
	beq $a2, 9, generate_digit_9
	end_set_cent_digit:
j end_generate_digits

generate_digit_0:
	digit_0($s6, $s7)
	beq $s5, 1, end_set_unit_digit
	beq $s5, 2, end_set_decimal_digit
	beq $s5, 3, end_set_cent_digit

generate_digit_1:
	digit_1($s6, $s7)
	beq $s5, 1, end_set_unit_digit
	beq $s5, 2, end_set_decimal_digit
	beq $s5, 3, end_set_cent_digit

generate_digit_2:
	digit_2($s6, $s7)
	beq $s5, 1, end_set_unit_digit
	beq $s5, 2, end_set_decimal_digit
	beq $s5, 3, end_set_cent_digit

generate_digit_3:
	digit_3($s6, $s7)
	beq $s5, 1, end_set_unit_digit
	beq $s5, 2, end_set_decimal_digit
	beq $s5, 3, end_set_cent_digit

generate_digit_4:
	digit_4($s6, $s7)
	beq $s5, 1, end_set_unit_digit
	beq $s5, 2, end_set_decimal_digit
	beq $s5, 3, end_set_cent_digit

generate_digit_5:
	digit_5($s6, $s7)
	beq $s5, 1, end_set_unit_digit
	beq $s5, 2, end_set_decimal_digit
	beq $s5, 3, end_set_cent_digit

generate_digit_6:
	digit_6($s6, $s7)
	beq $s5, 1, end_set_unit_digit
	beq $s5, 2, end_set_decimal_digit
	beq $s5, 3, end_set_cent_digit

generate_digit_7:
	digit_7($s6, $s7)
	beq $s5, 1, end_set_unit_digit
	beq $s5, 2, end_set_decimal_digit
	beq $s5, 3, end_set_cent_digit

generate_digit_8:
	digit_8($s6, $s7)
	beq $s5, 1, end_set_unit_digit
	beq $s5, 2, end_set_decimal_digit
	beq $s5, 3, end_set_cent_digit

generate_digit_9:
	digit_9($s6, $s7)
	beq $s5, 1, end_set_unit_digit
	beq $s5, 2, end_set_decimal_digit
	beq $s5, 3, end_set_cent_digit
