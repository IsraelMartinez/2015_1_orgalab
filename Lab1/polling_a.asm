.data

###################  
#  EXIT_PROGRAM	  #
###################
.macro EXIT_PROGRAM
	addi $v0, $zero, 10
	syscall                    
.end_macro

.macro PRINT_string_a0(%X)
	move $t7, $a0
	la $a0, %X
 	li $v0, 4
	syscall
	move $a0, $t7
.end_macro 
# Polling del teclado
.text
polling :
# Hacemos polling cada 100 ms
	addi $a0 , $zero , 1000
	addi $v0 , $zero , 32
# esperamos 100 ms para no utilizar todo el Procesador
	syscall
	addi $t0 , $zero, 0xffff0004
	lw $t1 , 0( $t0)
	sw $zero, 0( $t0 )
	beq $t1, 65 ,SALTAR
	
j polling
SALTAR:
