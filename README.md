# README #
Este es el repositorio para los laboratorios de Organización de Computadores primer semestre 2015.

# LAB1: Snake desarrollado en MIPS
 Se desarrolló con **MARS MIPS 4.5** Utiliza los módulos **MMIO** y **BITMAP DISPLAY**.

# LAB2: Algoritmos de Ordenamiento implementados en MIPS Bubblesort iterativo y Quicksort recursivo#
Se desarrolló con **MARS MIPS 4.5**, escribiendo y leyendo enteros desde archivo.